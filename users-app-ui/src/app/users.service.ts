import { Injectable } from '@angular/core';
import User from './shared/users';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Users from './shared/users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  userList: Array<User> = [
    {
      _id: 1,
      firstName: "John",
      middleName: "",
      lastName: "Doe",
      email: "john1@gmail.com",
      phoneNo: 1234567890,
      role: "Super Admin",
      address: "Los Angeles"
    },
    {
      _id: 2,
      firstName: "John",
      middleName: "Mark",
      lastName: "Doe",
      email: "john2@gmail.com",
      phoneNo: 1234567891,
      role: "Admin",
      address: "California"
    },
    {
      _id: 3,
      firstName: "John",
      middleName: "Mark",
      lastName: "Doe",
      email: "john3@gmail.com",
      phoneNo: 1234567892,
      role: "User",
      address: "Texas"
    }
  ]

  getAllUsers(): Observable<Users[]> {
    return this.http.get<Users[]>("http://localhost:3000/users")
  }

  addUser(userObj: User): Observable<any> {
    return this.http.post("http://localhost:3000/users", userObj);
  }

}
