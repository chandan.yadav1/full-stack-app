import { Component, OnInit } from '@angular/core';
import User from '../shared/users';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  constructor(private userService: UsersService) { }

  refreshStatus = false;
  tableStatus = false;
  // buttonTitle = 'Refresh Users';
  buttonTitle = 'Load Users';
  usersArray: Array<User> = [];
  usersToDisplay: Array<User> = [];
  errorMessage: string = "";

  ngOnInit(): void {

  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(
      response => this.usersArray = response,
      error => this.errorMessage = error.error.message
    )
  }

  handleUserAction() {
    if (this.buttonTitle == 'Load Users') {
      this.buttonTitle = 'Refresh Users';
      this.tableStatus = true;
      this.getAllUsers();
      // this.usersArray = userList;
      // this.usersToDisplay = userList;
    }
    else if (this.buttonTitle == 'Refresh Users') {
      this.getAllUsers();
      // this.usersToDisplay = userList;
      // console.log(this.usersToDisplay, this.usersArray);

    }
  }

  deleteUser(indexPosition: number) {
    this.usersArray.splice(indexPosition, 1)
  }

  updateUser(user: User, indexPosition: number) {
    user.enableEdit = false
    this.usersArray.splice(indexPosition, 1, user);
    // let newUserArray: User[] = this.usersArray.map(myUser => {

    //   if (myUser["_id"] === user["_id"]) {
    //     myUser = user
    //     myUser.enableEdit = false
    //   }
    //   return myUser;
    // })
    // this.usersArray = newUserArray;
  }

}
