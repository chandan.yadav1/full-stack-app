import { expect } from '@loopback/testlab';
import { givenEmptyDatabase } from '../../helpers/database.helpers';
import { UserController } from '../../../controllers';
import { UserRepository } from '../../../repositories';
import { testdb } from '../../fixtures/datasources/testdb.datasource';

describe('ProductController (integration)', () => {
    beforeEach(givenEmptyDatabase);

    describe('Testing find() API controller', () => {
        it('retrieves details of the given product', async () => {
            const controller = new UserController(new UserRepository(testdb));
            const details = await controller.find();
            expect(details).to.containEql([]);
        });
    });

    describe('Testing findById() API controller', () => {
        it('retrieves details of the given product', async () => {
            const controller = new UserController(new UserRepository(testdb));
            const details = await controller.findById(1);
            expect(details).to.containEql({});
        });
    });
});