import { Getter } from '@loopback/core';
import { RoleRepository, UserRepository } from '../../repositories';
import { testdb } from '../fixtures/datasources/testdb.datasource';

export async function givenEmptyDatabase() {
    let userRepository: UserRepository;
    let roleRepository: RoleRepository;

    userRepository = new UserRepository(
        testdb,
        // async () => roleRepository,
    );

    roleRepository = new RoleRepository(
        testdb,
        // async () => userRepository,
    );

    await roleRepository.deleteAll();
    await userRepository.deleteAll();
}